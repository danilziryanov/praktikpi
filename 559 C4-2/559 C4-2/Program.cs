﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _559_C4_2
{
    class Program
    {
        static bool Chek(int p)//функция проверки на простое число
        {
            if (p < 2) return false;
            for (int i = 2; i <= Math.Sqrt(p); i++)
            {
                if (p % i == 0) return false;
            }
            return true;
        }
        static void Main(string[] args)
        {
            int p
            Console.WriteLine("Введите натуральное число n: ");
            long n =long.Parse(Console.ReadLine());//вводим  n        
            int i;//индекс массива
            double d;//переменная числа мерсена
            for (i = 2; i <= n; i++)//цикл чисел мерсена
            {
                p = i;
                Program.Chek(p);
                if (Chek(p))
                {
                    d = Math.Pow(2, p) - 1;//число мерсена
                    if (d < n)//сравниваем число мерсена с заданным числом n
                        Console.WriteLine(d);//вывод на экран
                }
                                
            }
            Console.ReadKey();


        }
    }
}
