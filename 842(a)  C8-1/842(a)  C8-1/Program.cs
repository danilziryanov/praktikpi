﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _842_a___C8_1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите последовательность:");
            string str = Console.ReadLine();//вводим строку
            char[] mas = str.ToArray();
            Random rand = new Random();
            int c;//переменная случайного числа
            char x;//последний символ
            int n = str.Length;//длина строки
            for (int i = 0; i < n; i++)//цикл обхода по каждому символу
            {
                c = rand.Next(1, 4);//генерируем случайное число
                for(int j=0;j<c;j++)//цикл сдвига на случайное число
                {
                    x = str[n-1];//запоминаем послений элемент
                    for (int k =0; k < n-1; k++)//сдвигаем все элементы
                    {
                        mas[k+1] = str[k];
                        mas[0] = x;//первому присваиваем последнее
                    }
                }
            }
            Console.WriteLine("Зашифрованная последовательность:");
            for (int i = 0; i < n; i++)//вывод строки
                Console.Write(mas[i]);
            Console.ReadKey();
        }
      
    }
}
