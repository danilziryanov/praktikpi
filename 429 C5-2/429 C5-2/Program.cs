﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _429_C5_2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите размерность массива a ");
            byte n = Convert.ToByte(Console.ReadLine());//размерность массива а
            Console.WriteLine("Введите размерность массива b ");
            byte m = Convert.ToByte(Console.ReadLine());//размерность массива b
            int[] a = new int[n];//объявляем массив а 
            int[] b = new int[m];//объявляем массив b
            int[] c = new int[30];// объявляем массив c
            int i;//индекс массива
            double mina=0, minb=0, minc,maxc, l;
            Random rand = new Random();//рандомные числа
            Console.WriteLine("Массив a:");
            for (i = 0; i < n; i++)
            {
                a[i] = rand.Next(-100, 100);//заполняем рандомными числами
                Console.Write(a[i] + ";");
            }
            Console.WriteLine();
            Console.WriteLine("Массив b:");
            for (i = 0; i < m; i++)
            {
                b[i] = rand.Next(-100, 100);//заполняем рандомными числами
                Console.Write(b[i] + ";");
            }
            Console.WriteLine();
            Console.WriteLine("Массив с:");
            for (i = 0; i < 30; i++)
            {
                c[i] = rand.Next(-100, 100);//заполняем рандомными числами
                Console.Write(c[i] + ";");
            }
            mina = a[0];
            for (i = 0; i < n; i++)//цикл нахождения минимума в массиве а
                if (a[i] < mina)
                    mina = a[i];
            minb = b[0];       
            for (i = 0; i < m; i++)//цикл нахождения минимума в массиве b

                if (b[i] < minb)
                    minb = b[i];

            minc = 0;
            maxc = 0;
            for (i = 0; i < 30-1; i++)//цикл нахождения максимума и минимума в массиве с
                if (c[i] < c[i + 1])
                {
                    minc = c[i];
                    maxc = c[i + 1];

                }
                else
                {
                    maxc = c[i];
                    minc = c[i + 1];
                }
            Console.WriteLine();
            if (Math.Abs(mina) > 10)
            {
                l = minb + minc;
                Console.WriteLine("l = " + l);
            }
            else
            {
                l = 1 + Math.Pow(maxc, 2);
                Console.WriteLine("l = " + l);

            }
            Console.ReadKey();
        }
    }
}
