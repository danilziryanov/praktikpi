﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _407_C5_1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Введите число строк в матрице: ");
            int n = Convert.ToInt32(Console.ReadLine());//вводим количество строк в матрице
            Console.Write("Введите число столбцов в матрице: ");
            int m= Convert.ToInt32(Console.ReadLine());//вводим количество столбцов
            Console.Write("Введите действительное число r: ");
            double r = Convert.ToDouble(Console.ReadLine());
            double[,] b = new double[n, m];//объявляем двумерный массив
            double summ=0;//обнуляем сумму
            int k = n;
            for (int i = 0; i <n; i++)//цикл строк
                for (int j = 0; j < m; j++)//цикл столбцов
                    b[i, j] = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Вы ввели матрицу: ");
            for (int i = 0; i < n; i++)//цикл строк
            {
                for (int j = 0; j < m; j++)//цикл столбцов
                    Console.Write(b[i, j] + "\t");//вывод элементов
                Console.WriteLine();
            }
            bool found;

            for (int i = 0; i < n; i++)//цикл строк
            {
                found = false;//ложь
                for (int j = 0; j < m; j++)//цикл столбцов
                    if (b[i, j] > 0)//проверка условия
                    {
                        summ += b[i, j] * Math.Pow(r, k - 1);//суммируем

                        k--;//вычитаем

                        found = true;//истина

                        break;//выходим из цикла
                    }

                if (!found)//проверяем условие
                {
                    b[i, 1] = 0.5;//приравниваем элемент
                    summ += b[i, 1] * Math.Pow(r, k - 1);//суммируем
                    k--;//вычитае
                }
            }



            Console.WriteLine("Сумма равна : "+summ);

            Console.ReadKey();

            }

          

            }

        }
         

           
                   

