﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _102
{
    class Program
    {
        static void Main(string[] args)
        {
            double x0 = 1;//первый элемент
            double x;//второй элемент
            int i;//индекс массива
            for (i = 1; i < 100; i++)//цикл
            {
                x = (2 - Math.Pow(x0, 3)) / 5;//находим следующий элемент
                if (Math.Abs(x - x0) < Math.Pow(10, -5))//проверяем выполняется ли условие
                {
                    Console.WriteLine(Convert.ToString(x));//если условие выполняется печатаем элемент
                    break;//и сразу выходим из цикла
                }
                else x0 = x;//иначе первому элементу присваиваем второй
            }
            Console.ReadKey();

        }
    }
}
