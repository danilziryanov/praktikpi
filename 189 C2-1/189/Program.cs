﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _189
{
    class Program
    {
        static void Main(string[] args)
        {
            int n =Convert.ToInt32( Console.ReadLine());//Вводим n
            int i;//индекс массива
            int k = 0;//количество чисел, попадающих в интервал
            int otr = 0;//количество отрицательных чисел
            double[] a = new double[n];//объявляем массив 
            for (i = 0; i <= n-1; i++)//цикл ввода элементов массива
                a[i] = Convert.ToDouble(Console.ReadLine());//вводим элемент массива
            for (i = 0; i <= n - 1; i++)//цикл, определяющий попадание в интервал
                if (a[i] >= 0 && a[i] >= 1 && a[i] <= 2)//условие попадание в интервал
                {
                    k++;//увеличиваем на 1 количество чисел попавших в интервал
                }
                else if (a[i] < 0)//отрицательные элементы
                    otr++;//увеличиваем на 1 количество отрицательных элементов
                else a[i] = 1;
            Console.WriteLine("Отрицательных чисел:" + Convert.ToString(otr));//выводим количество отрицательных элементов
            Console.WriteLine("Чисел, входящих в промежуток:" + Convert.ToString(k));//выводим количсетво элементов, попавших в интервал
            for (i = 0; i <= n - 1; i++)//цикл вывода массива на экран
                Console.Write(Convert.ToString(a[i])+";");//выводим элемент массива на консоль
            Console.ReadKey();
                
            




        }
    }
}
