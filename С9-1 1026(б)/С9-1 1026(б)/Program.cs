﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace С9_1_1026_б_
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите количество человек в классе");
            int n = Convert.ToInt32(Console.ReadLine());//количество человек
            int i, j;//стрка, столбец
            i = n;//строк столько же сколько и человек
            double min1, min2, min3;
            double sum1 = 0, sum2 = 0, sum3 = 0, sred1, sred2, sred3;
            int par1=new int(), par2=new int(), par3=new int();
            int[,] a = new int[i, 4];
            Console.WriteLine("УЧЕНИК ВОЗРАСТ    РОСТ   УСПЕВАЕМОСТЬ");
            Random rand = new Random();
            for (i = 0; i < n; i++)//заполняем матрицу
            {
                a[i, 0] = i + 1;//номер ученика
                a[i, 1] = rand.Next(12, 30);//количество лет
                a[i, 2] = rand.Next(165, 180);//рост
                a[i, 3] = rand.Next(2, 6);//успеваемость
            }
            for (i = 0; i < n; i++)//вывод матрицы на экран
            {
                for (j = 0; j < 4; j++)
                    Console.Write(a[i, j] + "        ");
                Console.WriteLine();
            }
            for (i = 0; i < n; i++)//цикл подсчета суммы каждого из параметров(столбца)
            {
                sum1 += a[i, 1];//возраст
                sum2 += a[i, 2];//рост
                sum3 += a[i, 3];//успеваемость
            }
            sred1 = sum1 / n;//средний возраст
            sred2 = sum2 / n;//средний рост
            sred3 = sum3 / n;//средняя успеваемость
            min1 = 100;
            min2 = 200;
            min3 = 10;
            for (i = 0; i < n; i++)//цикл возраста
                if (Math.Abs(sred1 - a[i, 1]) < min1)//если модуль разности среднего значения возраста меньше минимального
                {
                    min1 = Math.Abs(sred1 - a[i, 1]);//приравниваем минимальное к этой разнице
                    par1 = i + 1;//запоинаем индекс
                }
            for (i = 0; i < n; i++)//цикл роста
                if (Math.Abs(sred2 - a[i, 2]) < min2)//приравниваем минимальное к этой разнице
                {
                    min2 = Math.Abs(sred2 - a[i, 2]);//приравниваем минимальное к этой разнице
                    par2 = i + 1;//запоминаем индекс
                }
            for (i = 0; i < n; i++)//цикл успеваемости
                if (Math.Abs(sred3 - a[i, 3]) < min3)// если модуль разности среднего значения возраста меньше минимального
                {
                    min3 = Math.Abs(sred3 - a[i, 3]);//прравниваем минимальное к разнице
                    par3 = i + 1;//запоминаем индекс
                }

            Console.WriteLine("СРЕДНИЙ УЧЕНИК ПО ВОЗРАСТУ: " + Convert.ToString(par1));
            Console.WriteLine("СРЕДНИЙ УЧЕНИК ПО РОСТУ: " + Convert.ToString(par2));
            Console.WriteLine("СРЕДНИЙ УЧЕНИК ПО УСПЕВАЕМОСТИ: " + Convert.ToString(par3));
            Console.ReadKey();
        }
    }
}
