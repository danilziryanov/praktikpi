﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _258_С3_1
{
    class Program
    {
        static void Main(string[] args)
        {
            int i,j,k;
            Console.WriteLine("Входная последовательность символов:");
            string str = Console.ReadLine();//Вводим последовательность символов в виде строки
            char[] s = str.ToCharArray();//переводим строку в массив символов
            int n = str.Length;  //определяем длину строки
            for (i=0; i<=n-1;i++) //цикл проверки последовтельности
                if (s[i]=='a' && s[i+1]=='b'  && s[i+2]=='c' && s[i+3]=='d')//проверяем наличие подрядидущих abcd
                {
                    for (k = 0; k <= 3; k++)//цикл перестановок
                        for (j = i; j <= n-2 ; j++)
                            s[j] = s[j+1];//перестановка
                    n = n - 4;//вычитаем количество убранных символов
                }
            Console.WriteLine("Выходная последовательность символов");
            for (i = 0; i <= n-1; i++)//цикл вывода измененной последовательности
                Console.Write(s[i]);
            Console.ReadKey();
                                  
        }
    }
}
