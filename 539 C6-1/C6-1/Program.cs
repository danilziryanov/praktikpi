﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C6_1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите количество элементов последовательности");
            int n = Convert.ToInt32(Console.ReadLine());//число элементов
            int i, m=0, l=0, k;
            int[] a = new int[n];//объявляем массив
            Random rand = new Random();
            Console.WriteLine("Исходная поледовательность:");
            for (i = 0; i < n; i++)//цикл ввода
            {
                a[i] = rand.Next(0, 100);//
                Console.Write(a[i] + ",");
                if (a[i] % 2 == 0)//проверка на четность
                    m++;//количество четных
                else l++;//количество нечетных
            }

            int[] x = new int[m];//массив четных
            int[] y = new int[l];//массив нечетных
            int j = 0,s = 0;//индексы массивов
            for (i = 0; i < n; i++)//цикл сортировки по четным и нечетным массивам
                if (a[i] % 2 == 0)
                {
                    x[j] = a[i];
                    if (j < m)//сравниваем индекс с количеством четных
                        j++;//+1
                    else break;//останавливаем цикл
                }
               else  if (a[i] % 2 != 0)//нечетность
                {
                    y[s] = a[i];
                    if (s < l)//сравниваем индекс с количеством нечетных
                        s++;//+1
                    else break;//останавливаем цикл
                }
            if (m < l)
                k = m;
            else k = l;
            Console.WriteLine();

            Console.WriteLine("Пары элементов:");
            for (i = 0; i < k; i++)
                Console.WriteLine(Convert.ToInt32(x[i]) + "," + Convert.ToInt32(y[i]));
            Console.ReadKey();
               
                
        }
    }
}
